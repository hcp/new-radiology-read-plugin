/*
 * xnat-radread-plugin: org.nrg.xnat.radread.plugin.RadReadXnatPlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.newradread.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.bean.NewradNewradiologyreaddataBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "newRadreadPlugin", name = "XNAT 1.7 New Rad Read Plugin",
            dataModels = {@XnatDataModel(value = NewradNewradiologyreaddataBean.SCHEMA_ELEMENT_NAME,
                                         singular = "New Radiology Read",
                                         plural = "New Radiology Reads")})
public class NewRadReadXnatPlugin {
}
